## Introduction

This documentation explains how I build this application and how to use it, in the following parts:
 * Application building
 * Application Dockerization
 * Starting application and usage scenario

## Project building

### Architectural Design

![](images/schema.PNG)

The architectural design choice is to decouple:
 * [fizzbuzz service](https://gitlab.com/l1307/fizz-buzz-service/-/tree/master/) which returns the fizzbuzz list 
 * [metrics service](https://gitlab.com/l1307/metrics-service) which expose metrics.
 * a Kafka service to transit data between both service
 * a PostgreSQL service to store the metrics

To ensure resilience of application. For example **fizzbuzz service** still working if **database** or **metrics application** is down.

### Business Rules
Before starting to implement both applications, the first step it to define **business rules**:
 * [fizzbuzz business rules](https://gitlab.com/l1307/fizz-buzz-service/-/blob/master/doc/business-rules/business-rules.md)
 * [metrics business rules](https://gitlab.com/l1307/metrics-service/-/blob/master/doc/business-rules/business-rules.md)

Then I design unit test from all those business rules. Each test must:
 * be independent of the kind of database or queue system (hexagonal architecture)
 * represent a case of a business rules

Those tests are in `src/test/java` of each application.

After designing tests, I implement the solution. The pattern is:
 * controller: API adapter for client
 * service: manage business rules
 * provider: adapter for queue or database

### Services configuration

It is possible to configure service by environment variables. Descriptions are here:
 * [Fizzbuzz config](https://gitlab.com/l1307/fizz-buzz-service/-/blob/master/doc/app-configuration/configuration.md)
 * [Metric config](https://gitlab.com/l1307/metrics-service/-/blob/master/doc/app-configuration/configuration.md)

## Project Dockerization
### Docker Compose
A [docker compose](https://gitlab.com/l1307/docker-compose) is provided to start the application. Images are:
 * zookeeper/broker/schema-registry: to supply a Kafka instance
 * database: a postgreSQL database with metrics schemes
 * metrics: metrics service
 * fizzbuzz: fizzbuzz service

Fizzbuzz image depends on the build of kafka broker, Metrics image depends on the build of kafka broker and database.

### Integration
Fizzbuzz and Metrics service include CI/CD on gitlab.
On any commit, CI/CD check that project builds and unit tests pass.

On commit on *master branch*, project is released with following actions:
 * clone [docker compose](https://gitlab.com/l1307/docker-compose)
 * copy compiled source of application on docker-compose sources
 * push to [docker compose](https://gitlab.com/l1307/docker-compose)

CI/CD results can be check [here](https://gitlab.com/l1307/fizz-buzz-service/-/pipelines) for fizzbuzz service and [here](https://gitlab.com/l1307/metrics-service/-/pipelines) for metrics service.

And the configuration is available [here](https://gitlab.com/l1307/fizz-buzz-service/-/blob/master/.gitlab-ci.yml)

## Starting application and usage scenario

### Starting application
First, clone [docker compose](https://gitlab.com/l1307/docker-compose).
```
git clone https://gitlab.com/l1307/docker-compose.git -b master
```
Then open it and build it:
```
docker-compose build
```
And start it:
```
docker-compose up -d
```
Now, wait that service are up:
```
docker logs fizzbuzz
docker logs metrics
```
Services are up when this message appears:
`Started FizzbuzzApplication in 30.931 seconds (JVM running for 38.739)`

### Usage scenario
First, clone [postman configuration](https://gitlab.com/l1307/postman).
```
git clone https://gitlab.com/l1307/postman.git -b master
```
Then open Postman and import `Fizz Buzz Collection.postman_collection.json`.

If you don't have postman, curl commands are available [here](./cmd/curl.md) 

#### Security

Go to `GET Metrics` or `POST Send Fizz Buzz`, change or delete *Authorization header*, service will return a 401 code.
For other scenario, keep original token.

#### Call Metrics Service without any data
Launch `GET Metrics` API Call, a 404 error will occur because there are no data to fetch:

![](images/notfoud.PNG)

#### Call Fizzbuzz Service with bad body
Go to `POST Send Fizz Buzz`, then replace any field by `null`. A 400 error will occur because a field isn't valid.

![](images/badrequest.PNG)

*Notice that `GET Metrics` API Call still return not found.*

#### Create a Fizzbuzz list
Go to `POST Send Fizz Buzz` then call with the following parameters:

![](images/fizzbuzzparams.PNG)

Then get this result:

![](images/fizzbuzzresult.PNG)

Now, data are available in Metrics application, launch `GET Metrics` API Call:

![](images/metrics_16_1.PNG)

Now go back to `POST Send Fizz Buzz` and send 2 times a body with `limit` at 17 (don't change other params).

`GET Metrics` API Call will return another result, because the maximum count of call changed:

![](images/metrics_17_2.PNG)

You can also check the logs from both services, all data send/receive from Kafka are logged by services.

### Resilience scenario

Now we will check resilience of this application.
Remember, actually, we have:
 * 2 occurrences with a `limit` of 17
 * 1 occurrences with same parameters, but with a `limit` of 16

Let's stop database:
```
docker stop database
```
Go to `POST Send Fizz Buzz` then post again a body with a `limit` at 17 (don't change other params).

FizzBuzz service isn't impacted by a down database (but Metrics application is unavailable). Then start the database.
```
docker start database
```
After a couple of seconds, call `GET Metrics`. Field `occurencesQuantity` is incremented (it may take some time to recreate connection to the database, so first call can still a 2, just recall):

![](images/metrics_17_3.PNG)

You can also try the same by stopping `metrics` image.

Conclusion: database can be down, FizzBuzz service still works and metrics about API calls are not loosed. 